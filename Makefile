include ${FSLCONFDIR}/default.mk

PROJNAME = giftiio
SOFILES  = libfsl-giftiio.so
LIBS     = -lexpat -lfsl-znz

all: gifti_tool gifti_test libfsl-giftiio.so

libfsl-giftiio.so: gifti_test.o gifti_io.o gifti_xml.o
	${CC} ${CFLAGS} -shared -o $@ $^ ${LDFLAGS}

gifti_tool: gifti_tool.o libfsl-giftiio.so
	$(CC) $(CFLAGS) -o $@ $< -lfsl-giftiio $(LDFLAGS)

gifti_test: gifti_test.o libfsl-giftiio.so
	$(CC) $(CFLAGS) -o $@ $< -lfsl-giftiio $(LDFLAGS)
