/*
 * This header file is intended for use in C++ projects. It imports gifti_io.h
 * into a "giftiio" namespace.
 */
#ifndef __GIFTIIO_H__
#define __GIFTIIO_H__

namespace giftiio {
extern "C" {
#include "gifti_io.h"
}}

#endif /* __GIFTIIO_H__ */
